class crea_index_php {
  #file { '/var/www/index.php':
  #  ensure => file,
  #  replace => true,
  #  content => 'Hello World';
  #}
  
  file { "/var/www/prod":
    ensure => "directory",
  }
  
  file { '/var/www/prod/index.php':
    ensure => file,
    replace => true,
    source  => "puppet:///modules/crea_index_php/index.php",
    require => File ["/var/www/prod"]
  }
  
  file { "/var/www/dev":
    ensure => "directory",
  }

  file { '/var/www/dev/info.php':
    ensure => file,
    replace => true,
    content => '<?php phpinfo();',
    require => File ["/var/www/dev"]
  }


}
